﻿using UnityEngine;
using System.Collections;

public class PageComboController : MonoBehaviour
{
	public Sprite[] buttons;
	public Sprite okSprite, failSprite;

	void Start ()
	{
		if (!ControllerManager.controllerAttached)
		{
			PageContent pc = GetComponent<PageContent>();
			pc.buttons = buttons;
			pc.okSprite = okSprite;
			pc.failSprite = failSprite;
		}
	}
}
