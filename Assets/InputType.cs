﻿using UnityEngine;
using System.Collections;

public class InputType : MonoBehaviour
{
	public GameObject leftKey;
	public GameObject rightKey;

	public GameObject leftTrigger;
	public GameObject rightTrigger;

	void Start ()
	{
		if (ControllerManager.controllerAttached)
		{
			leftKey.SetActive(false);
			rightKey.SetActive(false);
			leftTrigger.SetActive(true);
			rightTrigger.SetActive(true);
		}
		else
		{
			leftKey.SetActive(true);
			rightKey.SetActive(true);
			leftTrigger.SetActive(false);
			rightTrigger.SetActive(false);
		}
	}
}
