﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Threading;

public class BibbiaManager : MonoBehaviour
{
	static BibbiaManager _self;
	public static BibbiaManager Self
	{
		get
		{
			return _self;
		}
	}
	void Awake()
	{
		_self=this;
	}

	public int maxFillerPages=10;
	public string[] combos,fillers;

	public delegate void OnCombo(string combo);
	
	public KeyCombo comboChecker=null;
	
	public int leftCount=0,rightCount=0;
	
	[System.Serializable]
	public class Page
	{
		public string id;
		public string[] comboKeys;
		
		public Page(string _id)
		{
			id=_id;
		}
	}
	public Page[] pages;
	Dictionary<string, Page> comboPages=new Dictionary<string, Page>();
	
	OnCombo onCombo;
	
	public string[] comboKeys;//tasti possibili da usare per fare una combo

	public void requestCombo(string combo, OnCombo _onCombo)
	{
		onCombo=_onCombo;
		
		generateComboKeys(combo);
		
		comboChecker=new KeyCombo(combo, comboPages[combo].comboKeys);
		if(!moving)
			rebuildPagePrefab(null);
	}
	public void resetComboRequest()
	{
		onCombo=null;
		comboChecker=null;
	}

	void generateComboKeys(string combo)
	{
		int comboLen=4;
		string[] generatedKeys=new string[comboLen];
		//Random.seed=Mathf.FloorToInt(Time.time*1000f);
		
		for(int i=0;i<comboLen;i++)
		{
			int k=Random.Range(0,comboKeys.Length);
			generatedKeys[i]=comboKeys[k];		
		}
		
		comboPages[combo].comboKeys=generatedKeys;
	}
	
	void Start()
	{
		List<Page> tmpPages=new List<Page>();
		
		List<string> shuffledCombos=new List<string>(combos);
		shuffledCombos.Shuffle();
		
		foreach(string combo in shuffledCombos)
		{
			int fillerNumber=Random.Range(0, maxFillerPages);
			for(int i=0;i<fillerNumber;i++)
			{
				//add filler
				int f=Random.Range(0,fillers.Length);
				tmpPages.Add(new Page(fillers[f]));
			}
			Page pp=new Page(combo);
			tmpPages.Add(pp);
			comboPages.Add(combo, pp);
		}
		
		pages=tmpPages.ToArray();
		
		currentPageNumber=Mathf.RoundToInt(pages.Length/2f);
		
		foreach(string combo in combos)
		{
			generateComboKeys(combo);
		}
		
		
	}
	
	public SkeletonAnimation pageAnimation;
	float lastPageChange=0;
	
	int currentPageNumber=0;
	
	bool moveLeft;
	bool moving=false;
	
	void Update()
	{
		if(TitleScreenManager.Self!=null)
			return;
			
		if(comboChecker!=null)
		{
			if(comboChecker.Check())
			{
				onCombo(comboChecker.comboName);
				resetComboRequest();
			}
			
		}
		
		if(Input.GetAxis("Horizontal")<-0.2f && Time.time-lastPageChange>0.6f && currentPageNumber<pages.Length-1)
		{
			lastPageChange=Time.time;
			pageAnimation.AnimationName="";
			pageAnimation.AnimationName="animation";
			pageAnimation.transform.localScale=Vector3.one*pageAnimation.transform.localScale.y;
			
			currentPageNumber++;
			StartCoroutine(destroyPage(0.25f));
			moveLeft=true;
			moving=true;
			
			leftCount++;
			
			AudioManager.Self.playFlipPage();
		}
		if(Input.GetAxis("Horizontal")>0.2f && Time.time-lastPageChange>0.6f && currentPageNumber>0)
		{
			lastPageChange=Time.time;
			pageAnimation.AnimationName="";
			pageAnimation.AnimationName="animation";
			pageAnimation.transform.localScale=new Vector3(-1,1,1)*pageAnimation.transform.localScale.y;
			
			currentPageNumber--;
			StartCoroutine(destroyPage(0.25f));
			moveLeft=false;
			moving=true;
			
			rightCount++;
			
			AudioManager.Self.playFlipPage();
		}
		if(Time.time-lastPageChange>1f/6f)
		{
			moving=false;
		}
		
		if(Time.time-lastPageChange>0.32f && pagePrefab==null)
		{
			rebuildPagePrefab(moveLeft);
		} 
	}
	
	IEnumerator destroyPage(float waitTime)
	{
		yield return new WaitForSeconds(waitTime);
		
		Destroy(pagePrefab.gameObject);
		pagePrefab=null;
	}
	
	PageContent pagePrefab=null;
	
	void rebuildPagePrefab(bool? left)
	{
		if(pagePrefab!=null)
			Destroy(pagePrefab.gameObject);
			
		if(pages[currentPageNumber].comboKeys!=null)
		{
			pagePrefab=PageContent.createCombo(pages[currentPageNumber].id, pages[currentPageNumber].comboKeys);
			
		}
		else
		{
			pagePrefab=PageContent.createFiller(pages[currentPageNumber].id);
		}
		if(left!=null)
		{
			if(left.Value)
				pagePrefab.moveLeft();
			else
				pagePrefab.moveRight();
		}
	}
	
	public string getRandomCombo()
	{
		int i=Random.Range(0,combos.Length);
		AudioManager.Self.playCombo(i);
		return combos[i];
	}
}



public static class ThreadSafeRandom
{
	private static System.Random Local;
	
	public static System.Random ThisThreadsRandom
	{
		get { return Local ?? (Local = new System.Random(unchecked(System.Environment.TickCount * 31 + Thread.CurrentThread.ManagedThreadId))); }
	}
}

static class ListExtensions
{
	public static void Shuffle<T>(this IList<T> list)
	{
		int n = list.Count;
		while (n > 1)
		{
			n--;
			int k = ThreadSafeRandom.ThisThreadsRandom.Next(n + 1);
			T value = list[k];
			list[k] = list[n];
			list[n] = value;
		}
	}
}