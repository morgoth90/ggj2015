﻿using UnityEngine;
using System.Collections;

public class Devil : MonoBehaviour
{
	float rand=0,rand2=0;
	float startTime=0;
	void Start()
	{
		startTime=Time.time;
		rand=Random.Range(0f,3f);
		rand2=Random.Range(1f,1.5f);
		transform.parent=null;
	}

	void Update ()
	{
		transform.position+=Vector3.up*Time.deltaTime*2f*(rand2);
		transform.position+=Vector3.left*Mathf.Sin(rand+Time.time*2f)*Time.deltaTime;
		
		if(Time.time-startTime>10f)
			Destroy (gameObject);
	}
}
