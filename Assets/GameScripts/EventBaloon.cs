﻿using UnityEngine;
using System.Collections;

public class EventBaloon : MonoBehaviour
{
	public static EventBaloon create(string id)
	{
		GameObject go=(GameObject)Instantiate(Resources.Load("Baloon"));
		EventBaloon b=go.GetComponent<EventBaloon>();
		b.content.sprite=(Sprite)Resources.Load<Sprite>("events/"+id);
		return b;
	}
	
	public SpriteRenderer content;
	

}
