﻿using UnityEngine;
using System.Collections;

public class EventManger : MonoBehaviour
{
	static EventManger _self;
	public static EventManger Self
	{
		get
		{
			return _self;
		}
	}
	void Awake()
	{
		_self=this;
	}
	
	public float eventTime=3;

	float currentEventTime=0;
	string currentEvent="";
	
	float startTime=-1;
	
	void Start()
	{
		
		timeBar.timeScale=2f;
	}
	
	int errors=0;
	
	public GUIText scoreText;
	public SkeletonAnimation timeBar;

	void Update()
	{
		if(TitleScreenManager.Self!=null)
			return;
			
		if(startTime<0)
			startTime=Time.time;
	
		if(Input.GetKeyDown(KeyCode.Escape))
			Application.Quit();
	
		scoreText.text=Mathf.FloorToInt(Time.time-startTime).ToString();
	
		if(Time.time-currentEventTime>eventTime)
		{
			if(currentEvent!="")
			{
				Debug.LogError("fail "+currentEvent);
				errors++;
				
				PriestManager.Self.setAnimation("fail");
				
				ChiesaManager.Self.destroyBaloon();
				
				if(ChiesaManager.Self.currentI>=0)
				{
					ChiesaManager.Self.fedeli[ChiesaManager.Self.currentI].death(1f);
				}
				for(int i=1;i<errors;i++)
				{
					ChiesaManager.Self.deathRandom(Random.Range(1f,1.5f));
				}
				
				currentEvent=null;
			}
			
			string randomCombo=BibbiaManager.Self.getRandomCombo();
			currentEvent=randomCombo;
			ChiesaManager.Self.setupEvent(currentEvent);
			if(ChiesaManager.Self.currentI<0)
			{
				currentEvent="";
				ChiesaManager.Self.destroyBaloon();
				
				timeBar.AnimationName="";
				timeBar.loop=true;
				timeBar.Reset();
				
				//END GAME
				this.enabled=false;
				
				int maxScore=0;
				try{
					maxScore= PlayerPrefs.GetInt("maxScore");
				}
				catch
				{
				}
				if(int.Parse(scoreText.text)>maxScore)
				{
					PlayerPrefs.SetInt("maxScore", int.Parse(scoreText.text)); 
					PlayerPrefs.Save();
				}
				EndScreenManager.create();
				
				return;
			}
			
			currentEventTime=Time.time;
			timeBar.AnimationName="ui_hud_time-run";
			timeBar.loop=true;
			timeBar.Reset();
			BibbiaManager.Self.requestCombo(randomCombo,delegate(string combo)
			{
				AudioManager.Self.playOk();
			
				PriestManager.Self.setAnimation("point");
			
				currentEvent="";
				Debug.LogError(combo);
				ChiesaManager.Self.destroyBaloon();
				ChiesaManager.Self.respawnOne();
				
				timeBar.AnimationName="";
				timeBar.loop=true;
				timeBar.Reset();
			});
			
			
		}
	
	}
	
}
