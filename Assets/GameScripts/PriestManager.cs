﻿using UnityEngine;
using System.Collections;

public class PriestManager : MonoBehaviour
{

	static PriestManager _self;
	public static PriestManager Self
	{
		get
		{
			return _self;
		}
	}
	void Awake()
	{
		_self=this;
	}
	
	SkeletonAnimation anim;
	
	void Start()
	{
		anim=GetComponent<SkeletonAnimation>();
		anim.state.Complete+=delegate(Spine.AnimationState state, int trackIndex, int loopCount)
		{
			setAnimation("idle");
		};
	}
	
	public void setAnimation(string name)
	{
		if(name=="idle")
			anim.loop=true;
		else
			anim.loop=false;
		anim.AnimationName="anim_priest_"+name;
	}
	
}
