﻿using UnityEngine;
public class KeyCombo
{
	public string[] buttons;
	public int currentIndex = 0; //moves along the array as buttons are pressed
	
	public float allowedTimeBetweenButtons = 0f; //tweak as needed
	private float timeLastButtonPressed;
	
	public string comboName;
	
	string[] allKeys={"Button1","Button2","Button3","Button4"};
	
	public KeyCombo(string _comboName, string[] b)
	{
		comboName=_comboName;
		buttons = b;
	}
	
	 bool incorrectKey()
	 {
	 	foreach(string button in allKeys)
			if(button!=buttons[currentIndex] && Input.GetButtonDown(button))
				return true;
				
		return false;
	}
	
	//usage: call this once a frame. when the combo has been completed, it will return true
	public bool Check()
	{
		if(currentIndex<0)
			return false;
			
		if (Time.time > timeLastButtonPressed + allowedTimeBetweenButtons)
		{
			
			if (currentIndex < buttons.Length)
			{
				if(incorrectKey())
				{
					currentIndex=-1;
					AudioManager.Self.playWrongKey();
					return false;
				}
				if ((buttons[currentIndex] == "down" && Input.GetAxisRaw("Vertical") == -1) ||
				    (buttons[currentIndex] == "up" && Input.GetAxisRaw("Vertical") == 1) ||
				    (buttons[currentIndex] == "left" && Input.GetAxisRaw("Vertical") == -1) ||
				    (buttons[currentIndex] == "right" && Input.GetAxisRaw("Horizontal") == 1) ||
				    (buttons[currentIndex] != "down" && buttons[currentIndex] != "up" && buttons[currentIndex] != "left" && buttons[currentIndex] != "right" && Input.GetButtonDown(buttons[currentIndex])))
				{
					timeLastButtonPressed = Time.time;
					currentIndex++;
					
					AudioManager.Self.playOkKey();
				}
				
				if (currentIndex >= buttons.Length)
				{
					currentIndex = 0;
					return true;
				}
				else return false;
			}
		}
		
		return false;
	}
}