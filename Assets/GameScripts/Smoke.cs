﻿using UnityEngine;
using System.Collections;

public class Smoke : MonoBehaviour
{
	SkeletonAnimation anim;
	
	public static Smoke create()
	{
		GameObject go=(GameObject)Instantiate(Resources.Load("anim_smoke"));
		Smoke smoke=go.GetComponent<Smoke>();
		return smoke;
	}

	void Start ()
	{
		anim=GetComponent<SkeletonAnimation>();
		foreach (AtlasAsset aa in anim.skeletonDataAsset.atlasAssets)
		{
			if (aa != null)
				aa.Reset();
		}
		anim.state.Complete+=delegate(Spine.AnimationState state, int trackIndex, int loopCount)
		{
			Destroy(gameObject);
		};
	}

}
