﻿using UnityEngine;
using System.Collections;

public class ScaleSpaner : MonoBehaviour {

	float startTime=0;

	// Use this for initialization
	void Start ()
	{
		return;
		
		startTime=Time.time;
		transform.localScale=new Vector3(1,0,1);
	}
	
	// Update is called once per frame
	void Update ()
	{
		return;
		
		float t=Mathf.Pow( (Time.time-startTime)/0.2f, 4);
		transform.localScale=new Vector3(1,Mathf.Clamp01(t),1);
		if(t>1f)
			Destroy(this);
	}
}
