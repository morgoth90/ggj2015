﻿using UnityEngine;
using System.Collections;

public class FedeleManager : MonoBehaviour
{
	public void sendEvent(string ev)
	{
	}
	
	public bool status=true;
	
	SkeletonAnimation anim;
	
	void Start()
	{
		anim=GetComponent<SkeletonAnimation>();
		StartCoroutine(init(Random.Range(0f,1f)));
		
	}
	
	public void death(float delay=0)
	{
		AudioManager.Self.playDeath();
	
		status=false;
		if(delay>0f)
		{
			StartCoroutine(delayedDeath(delay));
			return;
		}
		 
		
		anim.loop=false;
		anim.timeScale=1;
		anim.AnimationName=name+"_kill";
		
		Smoke smoke=Smoke.create();
		smoke.transform.parent=transform;
		smoke.transform.localPosition=Vector3.up*1.5f-Vector3.forward*0.1f;
		
		StartCoroutine(delayedDisappear(0.4f));
	}
	public void respawn(float delay=0)
	{
		AudioManager.Self.playRevive();
	
		if(delay>0f)
		{
			StartCoroutine(delayedRespawn(delay));
			return;
		}
		
		status=true;
		
		SpawnEffect eff=SpawnEffect.create();
		eff.transform.parent=transform;
		eff.transform.localPosition=Vector3.up*1.5f-Vector3.forward*0.1f;
		
		StartCoroutine(delayedAppear(0.1f));
		
		
	}
	
	IEnumerator init(float waitTime)
	{
		yield return new WaitForSeconds(waitTime);
	
		setIdle();
	}
	
	IEnumerator delayedDisappear(float waitTime)
	{
		yield return new WaitForSeconds(waitTime);
			
		renderer.enabled=false;
	}
	IEnumerator delayedAppear(float waitTime)
	{
		
		yield return new WaitForSeconds(waitTime);
		
		renderer.enabled=true;
		status=true;
		gameObject.AddComponent<ScaleSpaner>();
		setIdle();
	}
	
	IEnumerator delayedCough(float waitTime)
	{
		if(status)
		{
			
			yield return new WaitForSeconds(waitTime);
		
			setCough();
		}
		//else
		//respawn();
	}
	
	IEnumerator delayedDeath(float waitTime)
	{
			
			yield return new WaitForSeconds(waitTime);
			
			death();
	}
	IEnumerator delayedRespawn(float waitTime)
	{
		yield return new WaitForSeconds(waitTime);
			
		respawn();
	}
	
	void setIdle()
	{
		if(!status)
			return;
		
		anim.loop=true;
		anim.timeScale+=Random.Range(-0.1f,0.1f);
		anim.AnimationName=name+"_idle";
		foreach (AtlasAsset aa in anim.skeletonDataAsset.atlasAssets)
		{
			if (aa != null)
				aa.Reset();
		}
		anim.Reset();
	}
	void setCough()
	{
		anim.loop=false;
		anim.timeScale=1;
		anim.AnimationName=name+"_cough";
		anim.state.Complete+=onCoughComplete;
		
		AudioManager.Self.playCough();
	}
	
	void onCoughComplete(Spine.AnimationState state, int trackIndex, int loopCount)
	{
		anim.state.Complete-=onCoughComplete;
		
		setIdle();
		
		//StartCoroutine(delayedDeath(1f));
	}
	
	public float delay=2;
	float lastCheck=0;
	void Update()
	{
		if(Time.time-lastCheck>delay)
		{
			lastCheck=Time.time;
			
			if(Random.Range(0f,100f)>95f)
			{
				StartCoroutine(delayedCough(Random.Range(0f,1f)));
			}
			
		}
	}
}
