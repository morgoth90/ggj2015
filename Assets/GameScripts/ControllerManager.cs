﻿using UnityEngine;
using System.Collections;

public class ControllerManager : MonoBehaviour 
{
	string[] controllers;

	private static bool _controllerAttached = false;
	public static bool controllerAttached
	{
		get{ return _controllerAttached;}
		set
		{
			_controllerAttached = value;

//			If the controller is attached during the game
//			if(_controllerAttached == true)
//			{
//			}
			 
		}
	}

	void Awake()
	{
		Update();
	}

	void Update()
	{
		controllers = Input.GetJoystickNames();

		if (controllers.Length > 0)
		{
			controllerAttached = true;
		}
	}
}
