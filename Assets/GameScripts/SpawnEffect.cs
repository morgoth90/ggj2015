﻿using UnityEngine;
using System.Collections;

public class SpawnEffect : MonoBehaviour
{

	SkeletonAnimation anim;
	
	public static SpawnEffect create()
	{
		GameObject go=(GameObject)Instantiate(Resources.Load("ainm_resurrect"));
		SpawnEffect eff=go.GetComponent<SpawnEffect>();
		return eff;
	}
	
	void Start ()
	{
		anim=GetComponent<SkeletonAnimation>();
		
		anim.state.Complete+=delegate(Spine.AnimationState state, int trackIndex, int loopCount)
		{
			Destroy(gameObject);
		};
	}
	
}
