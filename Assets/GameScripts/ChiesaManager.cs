﻿using UnityEngine;
using System.Collections;

public class ChiesaManager : MonoBehaviour
{
	static ChiesaManager _self;
	public static ChiesaManager Self
	{
		get
		{
			return _self;
		}
	}
	void Awake()
	{
		_self=this;
	}
	
	
	public FedeleManager[] fedeli;
	
	public Transform root;
	
	void Start()
	{
		fedeli=root.GetComponentsInChildren<FedeleManager>();
	}

	public int currentI=0;
	EventBaloon baloon=null;

	public void destroyBaloon()
	{
		if(baloon!=null)
			Destroy(baloon.gameObject);
	}

	public void respawnOne()
	{
		int i=Random.Range(0,fedeli.Length);
		
		int n=0;
		while(fedeli[i].status==true)
		{
			i=(i+1)%fedeli.Length;
			n++;
			if(n>=fedeli.Length)
			{
				return;
			}
		}
		fedeli[i].respawn(1f);
	}
	public void deathRandom(float delay)
	{
		int i=Random.Range(0,fedeli.Length);
		
		int n=0;
		while(fedeli[i].status==false)
		{
			i=(i+1)%fedeli.Length;
			n++;
			if(n>=fedeli.Length)
			{
				return;
			}
		}
		fedeli[i].death(delay);
	}

	public void setupEvent(string ev)
	{
		destroyBaloon();
	
		
		int i=Random.Range(0,fedeli.Length);
		
		int n=0;
		while(fedeli[i].status==false)
		{
			i=(i+1)%fedeli.Length;
			n++;
			if(n>=fedeli.Length)
			{
				currentI=-1;
				return;
			}
		}
		
		EventBaloon bal=EventBaloon.create(ev);
		
		currentI=i;
		
		bal.transform.parent=fedeli[i].transform;
		bal.transform.localPosition=new Vector3(0f,4.5f,-0.01f);
		
		baloon=bal;
	}

}
