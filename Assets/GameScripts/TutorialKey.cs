﻿using UnityEngine;
using System.Collections;

public class TutorialKey : MonoBehaviour
{
	int pressions=0;
	
	public int side=1;
	
	void Update ()
	{
		if(side>0)
		{
			GetComponent<SpriteRenderer>().color=new Color(1,1,1, 1f-Mathf.Clamp01(BibbiaManager.Self.leftCount/2.5f) );
		}
			
		if(side<0)
		{
			GetComponent<SpriteRenderer>().color=new Color(1,1,1, 1f-Mathf.Clamp01(BibbiaManager.Self.rightCount/2.5f) );
		}
	}
}
