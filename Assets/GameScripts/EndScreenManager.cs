﻿using UnityEngine;
using System.Collections;

public class EndScreenManager : MonoBehaviour
{
	static EndScreenManager _self;
	public static EndScreenManager Self
	{
		get
		{
			return _self;
		}
	}
	void Awake()
	{
		_self=this;
	}
	
	public static void create()
	{
		Instantiate(Resources.Load("ENDSCREEN"));
	}
	
	void Start ()
	{
		AudioManager.Self.playGameOver();
		
	}
	
	Vector3 endPos=new Vector3(0,20, -20);
	Vector3 startPos;
	float fadeTime=-1;
	
	public TextMesh scoreText;
	
	public Material quadMaterial;
	
	void Update ()
	{
		if(fadeTime<0)	
		{
			fadeTime=Time.time;
		
			startPos=transform.position;
			transform.position=endPos;
			quadMaterial.color=new Color(0,0,0,0.0f);
			
			scoreText.text=EventManger.Self.scoreText.text;
		}
		
		if(fadeTime>=0)
		{
			float t= Mathf.Pow((Time.time-fadeTime)/1f ,6);
			float tt=(Time.time-fadeTime-0.5f)/1.5f;
			transform.position=Vector3.Lerp(endPos, startPos, t );
		
			if(t>1f)
			{
				
				quadMaterial.color=Color.Lerp(new Color(0,0,0,0f),new Color(0,0,0,0.5f),Mathf.Clamp01(tt));
				
				if(Input.GetButtonDown("Button1"))
					Application.LoadLevel("main");
			}
		
			
		}
		
		
		
	}
}
