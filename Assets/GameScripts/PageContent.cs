﻿using UnityEngine;
using System.Collections;

public class PageContent : MonoBehaviour
{
	public GameObject left,right;

	public SpriteRenderer[] comboButtons;
	
	public Sprite[] buttons;
	public Sprite okSprite, failSprite;
	 
	public SpriteRenderer content;

	public static PageContent createCombo(string id, string[] keys)
	{
		GameObject go=(GameObject)Instantiate((GameObject)Resources.Load("PageCombo"));
		PageContent pc=go.GetComponent<PageContent>();
		pc.setCombo(keys);
		
		pc.content.sprite=(Sprite)Resources.Load<Sprite>("events/"+id);
		return pc;
	}
	public static PageContent createFiller(string name)
	{
		GameObject go=null;
		try{
			go=(GameObject)Instantiate((GameObject)Resources.Load("PageFiller"+name));
		}catch{Debug.LogError(name);}
		return go.GetComponent<PageContent>();
	}
	
	string[] combo;

	public void setCombo(string[] _combo)
	{
		combo=_combo;
		
	}
	
	void Update()
	{
		if(combo==null)
			return;
			
		for(int i=0;i<4;i++)
		{
			if(BibbiaManager.Self.comboChecker!=null && BibbiaManager.Self.comboChecker.currentIndex<0)
				comboButtons[i].sprite=failSprite;
			else if(BibbiaManager.Self.comboChecker!=null && i<BibbiaManager.Self.comboChecker.currentIndex)
				comboButtons[i].sprite=okSprite;
			else
			{
				switch(combo[i])
				{
				case "Button1":
					comboButtons[i].sprite=buttons[0];
					break;
				case "Button2":
					comboButtons[i].sprite=buttons[1];
					break;
				case "Button3":
					comboButtons[i].sprite=buttons[2];
					break;
				case "Button4":
					comboButtons[i].sprite=buttons[3];
					break;
				}
			}
			
		}
	}
	
	public void moveLeft()
	{
		left.SetActive(false);
		StartCoroutine(activeLeft(0.07f));
	}
	public void moveRight()
	{
		right.SetActive(false);
		StartCoroutine(activeRight(0.07f));
	}
	
	IEnumerator activeLeft(float waitTime)
	{
		yield return new WaitForSeconds(waitTime);
		
		left.SetActive(true);
	}
	IEnumerator activeRight(float waitTime)
	{
		yield return new WaitForSeconds(waitTime);
		
		right.SetActive(true);
	}
}
