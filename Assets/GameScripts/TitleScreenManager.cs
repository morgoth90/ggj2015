﻿using UnityEngine;
using System.Collections;

public class TitleScreenManager : MonoBehaviour
{
	static TitleScreenManager _self;
	public static TitleScreenManager Self
	{
		get
		{
			return _self;
		}
	}
	void Awake()
	{
		_self=this;
	}
	
	
	void Start ()
	{
		try{
		string line = PlayerPrefs.GetInt("maxScore").ToString();
		scoreText.text=line;
		}
		catch
		{
			scoreText.text="0";
		}
		
		startPos=transform.position;
		quadMaterial.color=new Color(0,0,0,0.5f);
	}
	
	Vector3 endPos=new Vector3(0,20, -20);
	Vector3 startPos;
	float fadeTime=-1;
	
	public TextMesh scoreText;
	
	public Material quadMaterial;
	
	void Update ()
	{
		if(Input.anyKeyDown && fadeTime<0)
		{
			fadeTime=Time.time;
		}
		
		if(fadeTime>=0)
		{
			float t= Mathf.Pow((Time.time-fadeTime)/1f ,6);
			float tt=(Time.time-fadeTime-0.5f)/1.5f;
			transform.position=Vector3.Lerp(startPos, endPos, t );
		
			if(t>1f)
			{
				
				quadMaterial.color=new Color(0,0,0,Mathf.Clamp01(tt));
			}
		
			if(t>1.5f)
			{
				_self=null;
				Destroy(gameObject);
			}
		}
		
	}
}
