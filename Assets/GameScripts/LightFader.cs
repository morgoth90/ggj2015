﻿using UnityEngine;
using System.Collections;

public class LightFader : MonoBehaviour
{
	float startAlpha;
	SpriteRenderer rend;
	
	// Use this for initialization
	void Start ()
	{
		rend=(SpriteRenderer)renderer;
		startAlpha=rend.color.a;
	}
	
	// Update is called once per frame
	void Update ()
	{
		Color c=rend.color;
		c.a=Mathf.Clamp01(startAlpha+Mathf.Cos(Time.time*1.5f)*0.18f);
	
		rend.color=c;
	}
}
