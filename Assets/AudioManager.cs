﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class AudioManager : MonoBehaviour
{
	static AudioManager _self;
	public static AudioManager Self
	{
		get
		{
			return _self;
		}
	}
	void Awake()
	{
		_self=this;
	}
	
	public AudioClip music;
	public AudioClip[] cough;
	public AudioClip[] ok;
	
	public AudioClip flip;
	
	public AudioClip death, revive;
	
	public AudioClip[] combos;

	void Start ()
	{
		Camera.main.audio.clip=music;
		Camera.main.audio.loop=true;
		Camera.main.audio.Play();
	}
	
	public void playCough()
	{
		Camera.main.audio.PlayOneShot(cough[Random.Range(0,cough.Length)]);
	}
	
	public void playOk()
	{
		Camera.main.audio.PlayOneShot(ok[Random.Range(0,ok.Length)]);
	}
	
	public void playDeath()
	{
		Camera.main.audio.PlayOneShot(death,0.5f);
	}
	public void playRevive()
	{
		Camera.main.audio.PlayOneShot(revive);
	}
	
	public void playCombo(int i)
	{
		Camera.main.audio.PlayOneShot(combos[i]);
	}
	public void playFlipPage()
	{
		Camera.main.audio.PlayOneShot(flip);
	}
	
	public AudioClip okKey,wrongKey;
	
	public void playOkKey()
	{
		Camera.main.audio.PlayOneShot(okKey);
	}
	public void playWrongKey()
	{
		Camera.main.audio.PlayOneShot(wrongKey);
	}
	
	public AudioClip gameOver;
	public void playGameOver()
	{
		Camera.main.audio.PlayOneShot(gameOver);
	}
}
